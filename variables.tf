variable "namespace" {
  type        = "string"
  default     = "sk"
  description = "Organization namespace"
}

variable "stage" {
  type        = "string"
  description = "Stage (short name), e.g. 'prd', 'stg', 'tst', 'dev'"
}

variable "environment" {
  description = "Stage (long name), e.g. 'production', 'staging', 'testing', 'development "
}

variable "name" {
  type        = "string"
  default     = "webapp"
  description = "Solution name"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "tags" {
  type = "map"

  default = {
    "Owner"  = "SkaleSys"
    "Office" = "Perth"
  }

  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "region" {
  type        = "string"
  default     = "ap-southeast-2"
  description = "AWS Region"
}

variable "acm_certificate_arn" {
  description = "Existing ACM Certificate ARN"
  default     = "arn:aws:acm:ap-southeast-2:096373988534:certificate/f17137d7-203a-42ce-a817-5b2642b0d2a6"
}

variable "main_domain" {
  description = "Domain name"
  default     = "skalesys.com"
}

variable "db_cluster" {
  description = "Enable database cluster"
  default     = "true"
}

variable "db_port" {
  description = "Database port"
  default     = "3306"
}

variable "db_instance_type" {
  description = "Database instance type"
  default     = "db.t2.medium"
}

variable "db_cluster_size" {
  description = "Database cluster size"
  default     = "2"
}

variable "db_admin_user" {
  description = "Database admin user"
  default     = "admin"
}

variable "db_admin_password" {
  description = "Database password"
}

variable "db_storage_encrypted" {
  description = "Database encrypts storage"
  default     = "true"
}

#---
# S3
#---

variable "s3_enabled" {
  default = "true"
}

variable "s3_attributes" {
  type        = "list"
  description = "Additional attributes"
  default     = ["statements"]
}

variable "s3_acl" {
  type        = "string"
  default     = "private"
  description = "The canned ACL to apply. We recommend `private` to avoid exposing sensitive information"
}

variable "s3_force_destroy" {
  type        = "string"
  default     = "false"
  description = "A boolean string that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
}

variable "s3_policy" {
  type        = "string"
  default     = ""
  description = "A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy."
}

variable "s3_versioning_enabled" {
  type        = "string"
  default     = "true"
  description = "A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket."
}

variable "s3_sse_algorithm" {
  type        = "string"
  default     = "AES256"
  description = "The server-side encryption algorithm to use. Valid values are `AES256` and `aws:kms`"
}

variable "s3_allowed_bucket_actions" {
  type        = "list"
  default     = ["s3:PutObject", "s3:PutObjectAcl", "s3:GetObject", "s3:DeleteObject", "s3:ListBucket", "s3:ListBucketMultipartUploads", "s3:GetBucketLocation", "s3:AbortMultipartUpload"]
  description = "List of actions the user is permitted to perform on the S3 bucket"
}

#----
# EC2
#-----

variable "frontend_asg_instance_type" {
  description = "Instance type to launch	"
  default     = "t2.micro"
}

variable "frontend_asg_min_size" {
  description = "The minimum size of the autoscale group	"
  default     = "1"
}

variable "frontend_asg_max_size" {
  description = "The maximum size of the autoscale group	"
  default     = "1"
}

variable "frontend_asg_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to '0' causes Terraform to skip all Capacity Waiting behavior"
  default     = "5m"
}

variable "frontend_asg_autoscaling_policies_enabled" {
  description = "Whether to create aws_autoscaling_policy and aws_cloudwatch_metric_alarm resources to control Auto Scaling"
  default     = "true"
}

variable "frontend_asg_cpu_utilization_high_threshold_percent" {
  description = "The value against which the specified statistic is compared"
  default     = "70"
}

variable "frontend_asg_cpu_utilization_low_threshold_percent" {
  description = "The value against which the specified statistic is compared"
  default     = "20"
}

variable "backend_asg_instance_type" {
  description = "Instance type to launch	"
  default     = "t2.micro"
}

variable "backend_asg_min_size" {
  description = "The minimum size of the autoscale group	"
  default     = "1"
}

variable "backend_asg_max_size" {
  description = "The maximum size of the autoscale group	"
  default     = "1"
}

variable "backend_asg_autoscaling_policies_enabled" {
  description = "Whether to create aws_autoscaling_policy and aws_cloudwatch_metric_alarm resources to control Auto Scaling"
  default     = "true"
}

variable "backend_asg_cpu_utilization_high_threshold_percent" {
  description = "The value against which the specified statistic is compared"
  default     = "70"
}

variable "backend_asg_cpu_utilization_low_threshold_percent" {
  description = "The value against which the specified statistic is compared"
  default     = "20"
}

variable "backend_asg_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to '0' causes Terraform to skip all Capacity Waiting behavior"
  default     = "5m"
}

#--------
# BASTION
#--------

variable "bastion_allowed_cidr_blocks" {
  description = "ALlowed CIDR blocks"
  type        = "list"

  # Skalesys's IP: 123.208.211.218
  default = ["123.208.211.218/32"]
}
